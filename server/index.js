import express from "express";
import bodyParser from "body-parser";
import { routes } from "./files/routes";

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json());
app.use(routes());

const host = "0.0.0.0" || "localhost";
const port = 5000;

app.listen(port, host, () => {
  console.log("up server on port " + port);
});

