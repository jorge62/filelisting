import fetch from 'node-fetch'

const urlFiles = 'https://echo-serv.tbxnet.com/v1/secret/files'
const urlData = 'https://echo-serv.tbxnet.com/v1/secret/file/'
const bearer = 'Bearer ' + 'aSuperSecretKey'
const options = {
  method: 'GET',
  headers: {
    Authorization: bearer,
    'Content-Type': 'application/json',
    'User-Agent': 'node-fetch'
  }
}

export const getFileData = async (request, response, next) => {
  response.set('Access-Control-Allow-Origin', '*')
  let resArray = []
  let fileName = ''
  // eslint-disable-next-line prefer-const
  let logErrorFile = {
    status: '',
    statusText: ''
  }
  if (typeof request.query.fileName !== 'undefined') {
    try {
      fileName = request.query.fileName
      if (!fileName.includes('csv')) fileName = fileName + '.csv'
      const res = await fetch(urlData + fileName, options)
      if (res.status === 200) {
        resArray = await getAllLines(res, fileName, resArray)
        if (resArray.length === 0) {
          logErrorFile.status = 'empty'
          logErrorFile.statusText = 'El archivo se encuentra vacio'
        }
      } else {
        console.log('Error data files ', res.status, res.statusText, fileName)
        logErrorFile.status = res.status
        logErrorFile.statusText = res.statusText
      }
    } catch (error) {
      console.log(error)
    }
  } else if(typeof request.query.fileName === 'undefined' && Object.keys(request.query).length !== 0){
      console.log("Not found parameter")
  } else{
    try {
      // eslint-disable-next-line no-undef
      const res = await fetch(urlFiles, options)
      const resFiles = await res.json()
      await Promise.all(
        resFiles.files.map(async (file, i) => {
          const res = await fetch(urlData + file, options)
          if (res.status === 200) {
            resArray = await getAllLines(res, file, resArray)
          } else {
            console.log('Error data files ', res.status, res.statusText, file)
          };
        })
      )
    } catch (error) {
      console.log(error)
    }
  }
  if (resArray.length !== 0) {
    response.send(resArray)
  } else {
    response.send(logErrorFile)
  }
}

export const getFiles = async (request, response, next) => {
  response.set('Access-Control-Allow-Origin', '*')
  try {
    const resFiles = await fetch(urlFiles, options)
    const res = await resFiles.json()
    response.send(res)
  } catch (error) {
    console.log('Error getFiles ', error)
    response.json('Error getFiles ', error)
  }
}

const getAllLines = async (res, fileName, resArray) => {
  let dataLines = []
  let data = await res.text()
  data = data.replace(/^\s*[\r\n]/gm, '')
  const arrayData = data.split('\n')
  await Promise.all(
  // eslint-disable-next-line array-callback-return
    arrayData.map((data, index) => {
      dataLines = getFormatData(data, index, dataLines, fileName)
    })
  )
  if (dataLines.length !== 0) {
    resArray.push({ file: fileName, lines: dataLines })
  }

  return resArray
}

const getFormatData = (data, index, dataLines, file) => {
  if (index !== 0) {
    data = Array.from(new Set(data.split(','))).toString()
    const matches = data.match(/,/g)
    const count = matches ? matches.length : 0
    let str = data.split(',')
    str = str.filter(item => item)
    if (count >= 3 && str.length === 4) {
      const nameFile = data.split(',', 1)[0]
      const text = data.split(',', 2)[1]
      const number = data.split(',', 3)[2]
      const hex = data.split(',')[3]

      if (file === nameFile) {
        dataLines.push({ text, number, hex })
      }
    }
  }
  return dataLines
}
