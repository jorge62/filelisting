import express from 'express'
import { getFileData, getFiles } from '../controllers/filesController'
const router = express.Router()

export const routes = () => {
  router.get('/files/data', getFileData)

  router.get('/files/list', getFiles)

  return router
}
