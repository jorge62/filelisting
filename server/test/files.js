import chai, { expect } from 'chai'
import chaiHttp from 'chai-http'
const should = chai.should()

chai.use(chaiHttp)

// eslint-disable-next-line no-undef
describe('Files', () => {
// eslint-disable-next-line no-undef
  describe('/GET files', () => {
    // eslint-disable-next-line no-undef
    it('it should GET all the files', (done) => {
      const url = 'http://localhost:5000'
      chai.request(url)
        .get('/files/list')
        .end((_error, res) => {
          res.should.have.status(200)
          expect(res.body.files.length).gt(0)
          should.exist(res.body)
          done()
        })
    })
  })
})
