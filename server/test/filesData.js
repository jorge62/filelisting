import chai, { expect } from 'chai'
import chaiHttp from 'chai-http'
const should = chai.should()

chai.use(chaiHttp)

// eslint-disable-next-line no-undef
describe('Files', () => {
// eslint-disable-next-line no-undef
  describe('/GET data from files', () => {
    // eslint-disable-next-line no-undef
    it('it should GET all data from the files', (done) => {
      const url = 'http://localhost:5000'
      chai.request(url)
        .get('/files/data')
        .end((_error, res) => {
          res.should.have.status(200)
          should.exist(res.body)
          expect(res.body.should.be.a('array'))
          // eslint-disable-next-line no-unused-expressions
          expect(res.body).to.not.be.empty
          // eslint-disable-next-line array-callback-return, no-unused-expressions
          res.body.map((e) => {
            expect(e.file).include('csv')
            // eslint-disable-next-line no-unused-expressions
            expect(e.lines).to.not.be.empty
          })
          done()
        })
    })
    // eslint-disable-next-line no-undef
    it('it should GET data from file by query', (done) => {
      const fileName = { fileName: 'test3.csv' }
      const url = 'http://localhost:5000'
      chai.request(url)
        .get('/files/data')
        .query(fileName)
        .end((_error, res) => {
          res.should.have.status(200)
          // eslint-disable-next-line no-unused-expressions
          expect(res.body).to.not.be.empty
          // eslint-disable-next-line array-callback-return
          res.body.map((e) => {
            expect(e.file).to.be.equal(fileName.fileName)
            expect(e.file).include('csv')
            // eslint-disable-next-line no-unused-expressions
            expect(e.lines).to.not.be.empty
          })
        })
      done()
    })
  })
})
