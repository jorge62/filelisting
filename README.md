<a name="readme-top"></a>

[![LinkedIn][linkedin-shield]][linkedin-url]

<!-- PROJECT LOGO -->
<br />
<div align="center">

  <h1 align="center">Full Stack JS - Code Challenge</h1>

</div>

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#docker">Docker</a></li>
    <li><a href="#docker-compose">Docker Compose</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->

## About The Project

### Built With

- [![React][react.js]][react-url]
- ![Node-Express]
- [![React-boostrap][react-boostrap]][react-boostrap-url]

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- GETTING STARTED -->

## Getting Started

### Prerequisites

- npm
  ```sh
  npm install npm@latest -g
  ```

### Installation

1. Clone the repo https

   ```sh
   git clone https://gitlab.com/jorge62/filelisting.git

   ```

2. Go to folder server

   ```sh
   npm install
   ```

3. Go to folder client

   ```sh
   npm install
   ```

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- USAGE EXAMPLES -->

## Usage

1. Go to folder server and Run
   ```sh
   npm start
   ```
2. Run Test

   ```sh
   npm run test
   ```

3. Go to folder client and Run
   ```sh
   npm start
   ```
   ```sh
   Go to browser at http://localhost:3000
   ```
4. Run Test

   ```sh
   npm run test
   ```

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- Docker -->

## Docker

1. Go to folder server

   ```sh
   npm install
   ```

   ```sh
   docker build -f dockerfile -t "image-name" .
   ```

   Run docker server

   ```sh
   docker run -p 5000:5000 "image-name"
   ```

2. Go to folder client

   ```sh
   npm install
   ```

   ```sh
   docker build -f dockerfile -t "image-name" .
   ```

   Run docker client

   ```sh
   docker run -p 3000:3000 "image-name"
   ```

3. then
   ```sh
   Go to browser at http://localhost:3000
   ```   

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- Docker Compose-->

## Docker-Compose

1. Prerequisites inside root server && root client

   ```sh
   npm install
   ```

2. In the root of the project
   ```sh
   docker-compose up --build
   ```
3. then
   ```sh
   Go to browser at http://localhost:3000
   ```
   

<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- CONTACT -->

## Contact

[jorge1920merida@gmail.com]

Project Link: [https://gitlab.com/jorge62/filelisting.git](https://gitlab.com/jorge62/filelisting.git)

<p align="right">(<a href="#readme-top">back to top</a>)</p>


[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[react-boostrap]: https://img.shields.io/badge/React--Boostrap-UI-blue
[react-boostrap-url]: https://react-bootstrap.github.io/
[linkedin-url]: https://www.linkedin.com/in/jgmerida/
[react.js]: https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB
[react-url]: https://reactjs.org/
[node-express]: https://img.shields.io/badge/Node--Express-backend-green
