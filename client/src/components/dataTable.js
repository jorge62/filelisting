import React, { useEffect, useState } from "react";
import Table from "react-bootstrap/Table";
import styles from "./styles.module.css";
import { useDispatch, useSelector } from "react-redux";
import {
  setListFiles,
  setFilterFiles,
} from "../store/slices/files";
import Dropdown from 'react-bootstrap/Dropdown';
import MyModal from "./layouts/modal";



const DataTable = () => {
  const [showFilter, setShowFilter] = useState(false);
  const [fileToSearch,setFileToSearch] = useState("");
  const [showAlert, setShowAlert] = useState(false);
  const [logError, setLogError] = useState({});
  const url = "http://localhost:5000/files/data";

  const dispatch = useDispatch();

  const { list, listFilter} = useSelector((state) => state.files);

  useEffect(() => {
      const fetchApi = async () => {
        const res = await fetch(url);
        const data = await res.json();
        dispatch(setListFiles(data))
      };
      fetchApi()    
  }, []);

  useEffect(() => {
    if(!fileToSearch.includes("csv")){
      const setList =  () => {
        setShowFilter(false)
      };
      setList()
     }
  }, [fileToSearch]);

  const handlerFileFilter = (e) => {
    e.preventDefault()
    setFileToSearch(e.target.value)
  }

  const handlerPress = async(e) => {
    if (e.key === "Enter" && fileToSearch.length !== 0) {
      const res = await fetch(`${url}?fileName=${fileToSearch.toLowerCase().trim()}`);
      const data = await res.json();
      if(!(data.status === 404 || data.status === 500 || data.status === 'empty')){
        dispatch(setFilterFiles(data))
        setShowFilter(true)
      }else{
        setLogError(data)
        setShowAlert(true)
      }
    }
  }

  const handleKeyDown = (e) => {
    if (e.key === 'Backspace') {
        setShowFilter(false)
    }
  }


return (
    <div className={styles.container_table}>
        {showAlert ? 
            <MyModal
              logError = {logError}
              show={showAlert}
              onHide={() => setShowAlert(false)}
            /> : ""
        }
      <Table striped bordered hover>
        <thead>
          <tr>
            <th><span className={styles.spanFile}>File Name</span>
            <div className={styles.containerDrop}>
                <Dropdown>
                  <Dropdown.Toggle style={{ backgroundColor: "white", color: 'black' }} id="dropdown-basic" size="sm">
                       <img src="/filter_2.svg" className={styles.svgfilter}/>
                  </Dropdown.Toggle>
                 
                  <Dropdown.Menu>
                     <input placeholder="Filter by files" onChange={(e)=>handlerFileFilter(e)} onKeyDown={(e)=>handleKeyDown(e)} onKeyPress={(e)=>handlerPress(e)}/>
                  </Dropdown.Menu>
                </Dropdown>
            </div>
            </th>
            <th>Text</th>
            <th>Number</th>
            <th>Hex</th>
          </tr>
        </thead>
        <tbody>
          {list.length !== 0 && !showFilter
            ? list.map((data) => {
                return data.lines.map((dataLines,index) => {
                  return (
                    <tr key={index}>
                      <td>{data.file}</td>
                      <td>{dataLines.text}</td>
                      <td>{dataLines.number}</td>
                      <td>{dataLines.hex}</td>
                    </tr>
                  );
                });
              })
            : listFilter.length !== 0
                ? listFilter.map((data) => {
                    return data.lines.map((dataLines,index) => {
                      return (
                        <tr key={index}>
                          <td>{data.file}</td>
                          <td>{dataLines.text}</td>
                          <td>{dataLines.number}</td>
                          <td>{dataLines.hex}</td>
                        </tr>
                      );
                    });
            }) : null}
        </tbody>
      </Table>
    </div>
  );
};

export default DataTable;
