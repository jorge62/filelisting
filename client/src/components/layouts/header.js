import React from "react";
import styles from "./styles.module.css";

const Header = () => {
  return <div className={styles.header}>React Test App</div>;
};

export default Header;
