import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import styles from "./styles.module.css";

const MyModal = (props) => {
  return (
    <Modal
      onHide = {props.onHide}
      show = {props.show}
      size="md"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      dialogClassName={styles.modal}
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Error
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <p>
          {props.logError.statusText}
        </p>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="danger" onClick={props.onHide}>Cerrar</Button>
      </Modal.Footer>
    </Modal>
  );
}

export default MyModal;