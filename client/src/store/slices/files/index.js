import { createSlice } from "@reduxjs/toolkit";

export const fileSlice = createSlice({
  name: "files",
  initialState: {
    list: [],
    listFilter:[],
  },
  reducers: {
    setListFiles: (state, action) => {
      state.list = action.payload;
    },
    setFilterFiles: (state, action) => {
      state.listFilter = action.payload;
    },
  },
});

export const { setListFiles } = fileSlice.actions;
export const { setFilterFiles } = fileSlice.actions;

export default fileSlice.reducer;
