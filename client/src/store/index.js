import { configureStore } from "@reduxjs/toolkit";
import files from "./slices/files";

export default configureStore({
  reducer: {
    files,
  },
});
