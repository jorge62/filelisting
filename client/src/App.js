import Header from './components/layouts/header';
import DataTable from "./components/dataTable";
import { Provider } from "react-redux";
import store from "../src/store";

const App = () => {
  return (
    <Provider store={store}>
      <div>
        <Header />
        <DataTable />
      </div>
    </Provider>
  );
}

export default App;

