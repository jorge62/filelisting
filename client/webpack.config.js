const path = require('path');

module.exports = {
  // APP ENTRY POINT
  entry: path.join(__dirname,'src','index.js'),

  // OUTPUT DIRECTORY
  output: {
    path: path.join(__dirname,'public'),
    filename: 'main.bundle.js'
  },

  // EVIROMENT MODE
  mode: process.env.NODE_ENV || 'development',

  // PATH RESOLVE
  resolve: {
    modules: [path.resolve(__dirname, 'src'), 'node_modules']
  },

  // DEV SERVER ENTRY POINT
  devServer: {
    // contentBase
    static : {
      directory : path.join(__dirname, "public/")
    },
    
    port: 3000,
    // publicPath
    
    // hotOnly
    hot: "only",
     
  },
  module: {
    rules: [
      
      {
        test: /\.svg$/,
        use: [
          {
            loader: 'svg-url-loader',
            options: {
              limit: 10000,
            },
          },
        ],
      },
      { test: /\/src\/js\/(?:.*)\.css$/, use: [ { loader: 'style-loader' }, { loader: 'css-loader' } ] },
      {
        test: /\.css$/,
        // exclude: /(node_modules)/, // Remove this 
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' },
        ],
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader',
            options: {
              root: path.resolve(__dirname),
            },
          },
        ],
      },
      {
        test: [
           /\.(js|jsx)$/,
        ],
        use: {
          loader: 'babel-loader',
        },
      },
      
    ],
  },
};
